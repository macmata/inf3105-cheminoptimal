CC=g++
CFLAGS=-c -Wall -Wextra
LDFLAGS=
SOURCES=tp3.cpp astar.cpp carte.cpp coodonnee.cpp etat.cpp sommet.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=tp3

all : $(SOURCES) $(EXECUTABLE)
	$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.oi :
	$(CC) $(CFLAGS) $< -o $@


clean :
	rm -f *.o
	rm -f tp3
	rm -f *~
