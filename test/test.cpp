#include <gtest/gtest.h>
#include "../src/coordonnee.h"
#include "../src/sommet.h"
#include "../src/etat.h"
#include <unordered_set>
#include <queue>
#include <map>


namespace {

    TEST(Test1, CoordonneEqal) {
        Coordonnee c(2.0,2.0);
        Sommet s;
        s.ajoutCoord("a", c);
        Sommet sp;
        sp = s;
        ASSERT_EQ(s,sp);
    }

    TEST(Test2, EtatEqual) {
        Coordonnee c(2.0,2.0);
        Sommet s;
        s.ajoutCoord("a", c);
        Sommet sp;
        sp = s;
        ASSERT_EQ(s,sp);
        Etat etat(s);
        Etat etat1;
        etat1 = etat;
        ASSERT_EQ(etat, etat1);
    }

    TEST(Test3, EtatParentEqual) {
        Coordonnee c(2.0,2.0);
        Sommet s;
        s.ajoutCoord("a", c);
        Sommet sp;
        sp = s;
        ASSERT_EQ(s,sp);
        Etat etat(s);
        Etat etat1(sp);
        etat.appliquePtrParent(&etat1);
        ASSERT_EQ(etat.parent, &etat1);
    }

    TEST(Test4, EtatParentEqualPriorityQ) {
        Coordonnee c(2.0,2.0);
        Sommet s;
        s.ajoutCoord("a", c);
        Sommet sp;
        sp = s;

        Etat etat(s);
        Etat etat1;
        etat1 = etat;
        ASSERT_EQ(etat, etat1);

        priority_queue<Etat>priorityQueue;
        priorityQueue.push(etat);
        Etat testEtat(priorityQueue.top());
        ASSERT_EQ(testEtat, etat);
        ASSERT_TRUE(testEtat == etat);
        Etat testEtat2 = priorityQueue.top();
        priorityQueue.pop();
        ASSERT_EQ(testEtat2, etat);
        ASSERT_TRUE(testEtat2 == etat);

    }

    TEST(Test5, TESTMAPSET) {
        map<string , set<string> > map;
        string route = "route1";
        string label =  "label1";
        map[route].insert(label);

        set<string>::const_iterator str_ = map[route].find(label);

        ASSERT_NE(str_,map[route].end());
        ASSERT_EQ(*str_,label);
    }
}