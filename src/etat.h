#if !defined(_ETAT_)
#define _ETAT_

#include <iostream>
#include <list>
#include "sommet.h"

using namespace std;

class Etat{
public:
    Etat();
    Etat(const Etat&);
    Etat(Sommet*);

    bool operator < (const Etat&)const;
    bool operator > (const Etat&)const;
    bool operator == (const Etat&)const;
    Etat & operator = (const Etat&);

    std::size_t toHash()const;
    string getNom()const;
    double getG()const;

    bool satisfait(const Etat but )const;
    void appliquePtrParent(Etat*);
    void appliqueG(Etat &);
    void appliqueH(Etat &);
    void appliqueF();

    Etat *parent;
private:
    double f;
    double g;
    double h;
    Sommet * sommet;
    string nom_sommet;

};

#endif

