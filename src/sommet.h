#ifndef SOMMET
#define SOMMET

#include "coordonnee.h"
#include <set>
#include <list>
#include <map>

using namespace std;
class Sommet{
public:
    Sommet();
    Sommet(const Sommet &);

    void ajoutCoord(string , const Coordonnee &);
    double distance(Sommet & )const;
    string getNom()const;

    bool operator == (const Sommet&)const;
    Sommet& operator = (const Sommet&);

    map<string,string> voisins;

private:
    string nom;
    Coordonnee coor;
};

#endif