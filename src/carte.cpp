/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Automne 2014
 *  Squelette pour le TP3
*/

#include <unordered_map>
#include "carte.h"
#include "aStar.h"
using namespace std;

void Carte::ajouterCoor(const string &str_sommet, const Coordonnee &coord){
    map_sommets[str_sommet].ajoutCoord(str_sommet, coord);
}
int Carte::taille() {
    return map_sommets.size();
}
void Carte::ajouterVoisins(const string& str_route, const list<string>&list_route){

    list<string>::const_iterator iter1=list_route.begin();
    if(iter1==list_route.end()) return;
    list<string>::const_iterator iter2=iter1;
    ++iter2;
    while(iter2!=list_route.end()){
        map_sommets[*iter1].voisins[*iter2]=str_route;
        iter1 = iter2;
        ++iter2;
    }
};

map<string,string>* Carte::retourneVoisins(const Etat &e){
    string str = e.getNom();
    return &map_sommets[str].voisins;
}

Sommet * Carte::retourneSommet(const string lieu) {
    return &map_sommets[lieu];
}

double Carte::calculerTrajet(const string origine, list<string> noeuds) {
    list<string>::const_iterator iter_buts = noeuds.begin();

    Sommet depart = map_sommets[origine];
    Sommet suivant;
    double d = 0.0;
    for(; iter_buts!=noeuds.end(); iter_buts++){

        suivant =  map_sommets[*iter_buts];
        d += depart.distance(suivant);
        depart = suivant;
    }
   return d;
}

istream& operator >> (istream& is, Carte& carte)
{
    // Lire les lieux
    while(is){
        string nomlieu;
        is >> nomlieu;
        if(nomlieu == "---") break;
        Coordonnee coor;
        is >> coor;
        carte.ajouterCoor(nomlieu, coor);
    }

    // Lire les routes
    while(is){
        string route;
        is >> route;
        if(route == "---" || route =="" || !is) break;
        
        char deuxpoints;
        is >> deuxpoints;
        assert(deuxpoints == ':');
        
        std::list<std::string> list_lieux;

        string lieu;
        while(is){
            is>> lieu;
            if(lieu ==";") break;
            assert(lieu !=":");
            assert(lieu.find(";")==string::npos);
            list_lieux.push_back(lieu);
        }
        
        assert(lieu ==";");
        carte.ajouterVoisins(route, list_lieux);
    }
    
    return is;
}



Carte::p_queue Carte::prioriseDistance(string const &str_depart,  list<string> &buts) {

    list<string>::const_iterator iter_buts = buts.begin();
    p_queue q;

    for(; iter_buts!=buts.end(); iter_buts++){
        list<string>newlist = AStar::genereSolution(*this,str_depart, *iter_buts);
        double d = calculerTrajet(str_depart, newlist);
        Conteneur c(d,*iter_buts,newlist);
        q.push(c);

    }
    return q;
}


void Carte::genereRoute(const list<string> noeuds) {
    list<string>::const_iterator i_prochain_noeud =  noeuds.begin();
    list<string> routes;

    string noeud = *i_prochain_noeud;
    i_prochain_noeud++;

    string deniere_route = "";
    for (; i_prochain_noeud !=noeuds.end(); i_prochain_noeud++){

        string route = map_sommets[noeud].voisins[*i_prochain_noeud];
        if (deniere_route != route) {
            cout << route << " ";
            deniere_route = route;
        }
        noeud = *i_prochain_noeud;
    }
}
//////------------------------------------------------------------------------------


Carte::Conteneur::Conteneur(double d, string const str, list<string> &list):distance(d),str_sommet(str) {
    chemin =list;
}
Carte::Conteneur::Conteneur(double d, string str):distance(d),str_sommet(str)  {

}

bool Carte::Conteneur::operator<(Carte::Conteneur const &d) const {
    return distance < d.distance;
}

bool Carte::Conteneur::operator>(Carte::Conteneur const &d) const {
    return distance > d.distance;
}

bool Carte::Conteneur::operator==(Carte::Conteneur const &d) const {
    return distance == d.distance;
}

string Carte::Conteneur::permier() const {
    return str_sommet;
}

list<string> Carte::Conteneur::second() {
    return chemin;
}

list<string> Carte::tsp(const string str_depart ,list<string>destinations ) {
    string noeud = str_depart;
    Carte::p_queue pQueue= prioriseDistance(noeud, destinations);
    list<string> chemin_noeuds;
    while(!pQueue.empty()){
        Carte::Conteneur c  = pQueue.top();

        chemin_noeuds.splice(chemin_noeuds.end(), c.second());
        noeud = c.permier();
        destinations.remove(noeud);
        pQueue = prioriseDistance(noeud, destinations);

    }
    chemin_noeuds.push_front(str_depart);
    list<string>newlist = AStar::genereSolution(*this, noeud,str_depart);
    chemin_noeuds.splice(chemin_noeuds.end(),newlist);

    return chemin_noeuds;

}
