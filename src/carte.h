/*  INF3105 - Structures de données et algorithmes
 *  UQAM / Département d'informatique
 *  Automne 2014
 *  Squelette pour le TP3
*/

#ifndef CARTE_HEADER
#define CARTE_HEADER
#include <assert.h>
#include <istream>
#include <list>
#include <map>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <stack>
#include "etat.h"
#include "coordonnee.h"
#include "sommet.h"
using namespace std;

class Carte{


    public :
        class Conteneur {
        public:
            Conteneur(double,const string);
            Conteneur(double,const string, list<string>&);
            bool operator == (const Conteneur &) const;
            bool operator < (const Conteneur &) const;
            bool operator > (const Conteneur &) const;
            string permier() const;
            list<string> second();

        private:
            list<string> chemin;
            double distance;
            string str_sommet;
        };
        typedef priority_queue<Carte::Conteneur,vector<Carte::Conteneur>,greater<Carte::Conteneur> >p_queue;

        void ajouterCoor(const string &nom, const Coordonnee &c);
        void ajouterVoisins(const string& nom, const list<string>&list_route);

        map<string,string>*retourneVoisins(const Etat &);
        Sommet * retourneSommet(const string);
        p_queue prioriseDistance(const string& ,list<string>&);

        int taille();

        void genereRoute(const list<string>);
        double calculerTrajet(const string origine, list<string> noeuds);

        list<string>tsp(const string ,list<string> );
    private:
        unordered_map<string,Sommet>  map_sommets;
        friend istream& operator >> (istream& is, Carte& carte);


};

#endif

