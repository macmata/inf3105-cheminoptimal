#include "sommet.h"
#include "coordonnee.h"
double Sommet::distance(Sommet &sommet) const {
    return coor.distance(sommet.coor);
}

Sommet::Sommet():voisins(),nom(""),coor(){

}

string Sommet::getNom() const{
    return nom;
}

Sommet::Sommet(const Sommet &sommet): voisins(sommet.voisins),nom(sommet.nom), coor(sommet.coor){

}

bool Sommet::operator==(const Sommet &sommet)const{
    return nom == sommet.nom && coor == sommet.coor ;

}

void Sommet::ajoutCoord(string sommet, const Coordonnee & coordonnee) {
    coor = coordonnee;
    nom = sommet;
}

Sommet &Sommet::operator=(const Sommet &s){
    coor = s.coor;
    nom = s.nom;
    voisins = s.voisins;
    return *this;
}
