#ifndef __A_STAR__
#define __A_STAR__
#include  "carte.h"
#include <list>
#include <queue>
#include <unordered_set>
#include "coordonnee.h"

using namespace std;
class AStar{
public:
    list<string>  static genereSolution(Carte & carte,const string depart, string list_coordonnees);
    list<string> static extraitChemin(Etat &, list<string>);
};


#endif

