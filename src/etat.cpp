#include "etat.h"
using namespace std;

Etat::Etat(Etat const &e) :f(e.f), g(e.g),h(e.h),sommet(e.sommet), nom_sommet(e.nom_sommet) {
    parent = e.parent;
}
Etat::Etat():f(0.0), g(0.0),h(0.0), nom_sommet(){
    parent = nullptr;
    sommet = nullptr;
}
Etat::Etat(Sommet *ptrSommet):f(0.0), g(0.0),h(0.0), nom_sommet(ptrSommet->getNom()) {
    sommet = ptrSommet;
    parent = nullptr;
}

bool Etat::operator>(Etat const &e) const {
    return f > e.f;
}
bool Etat::operator<(Etat const &e) const{
    return f < e.f;
}

bool Etat::operator==(Etat const &e)const {
    return sommet == e.sommet && nom_sommet == e.nom_sommet;
}

Etat &Etat::operator=(Etat const &e) {
    f = e.f;
    g = e.g;
    h = e.h;
    sommet = e.sommet;
    parent = e.parent;
    nom_sommet = e.nom_sommet;

    return *this;
}

string  Etat::getNom() const{
    return nom_sommet;
}

void Etat::appliqueG(Etat &etat) {
    g = (sommet->distance(*etat.sommet)+ etat.g);

}

void Etat::appliqueH(Etat &but) {
   h =  sommet->distance(*but.sommet);
}

void Etat::appliqueF() {
    f = g+h;
}


void Etat::appliquePtrParent(Etat *pEtat) {
    parent = pEtat;
}

bool Etat::satisfait(const Etat but) const {
    return sommet == but.sommet;
}

std::size_t Etat::toHash()const {
    std::hash<string> hash_str;
    return hash_str(sommet->getNom());
}

double Etat::getG() const {
    return g;
}
