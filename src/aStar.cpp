#include "aStar.h"
#include <list>
using namespace std;


struct Hash {
    std::size_t operator()(const Etat& k) const {
        return k.toHash();
    }
};

struct Equal {
    bool operator()(const Etat& lhs, const Etat& rhs) const {
        return lhs==rhs;
    }
};

list<string>AStar::genereSolution(Carte &carte, const string str_depart,  string str_but) {

    priority_queue<Etat, std::vector<Etat>, greater<Etat> >priorityQueue;
    unordered_map<string,Etat>closeSet;
    unordered_map<string,Etat>openSet;
    list<string> list;

    openSet.reserve(carte.taille()*1.5);
    closeSet.reserve(carte.taille()*1.5);

    Etat depart(carte.retourneSommet(str_depart));

    Etat but(carte.retourneSommet(str_but));

    priorityQueue.push(depart);

    openSet[depart.getNom()]= depart;

    while(!priorityQueue.empty()){
        unordered_map<string,Etat>::const_iterator iter_map;

        Etat* etat_courant = new Etat(priorityQueue.top());

        priorityQueue.pop();

        openSet.erase((*etat_courant).getNom());

        if(etat_courant->satisfait(but)&& !closeSet.empty()){
            return extraitChemin(*etat_courant, list);
        }

        closeSet[(*etat_courant).getNom()] = (*etat_courant);

        std::map<string,string>* list_enfants = carte.retourneVoisins(*etat_courant);

        std::map<string,string>::const_iterator iter_str_enfants = list_enfants->begin();

        for(;iter_str_enfants != list_enfants->end(); iter_str_enfants ++){

            string futur_str= (*iter_str_enfants).first;

            iter_map = closeSet.find(futur_str);

            if(iter_map != closeSet.end())continue;

            Etat *futur_etat = new Etat(carte.retourneSommet(futur_str));

            iter_map = openSet.find(futur_str);

            bool nonPresent = (iter_map ==openSet.end());

            futur_etat->appliqueG(*etat_courant);

            bool meileurFutur = false;
            if (!nonPresent){
               meileurFutur = (futur_etat->getG() < (*iter_map).second.getG());
            }

            if(nonPresent || meileurFutur){

                futur_etat->appliqueH(but);
                futur_etat->appliqueF();
                futur_etat->appliquePtrParent(etat_courant);
                openSet[(*futur_etat).getNom()]=*futur_etat;
                priorityQueue.push(*futur_etat);
            }
        }
    }

    return list;
}

list <string> AStar::extraitChemin(Etat &etat, list <string> route){
    Etat *e = etat.parent;
    route.push_back(etat.getNom()); // push le nom du but
    while(e->parent!= nullptr){
        route.push_back(e->getNom());// le point juste avant le point d'arriver
        Etat *a = e->parent;
        e = a;
    }
    route.reverse();
    return route;
}
